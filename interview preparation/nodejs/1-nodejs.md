# Reference :: https://www.youtube.com/watch?v=4JD0lbdmFX

<!-- 1.Node -Basic -->

1. What is nodejs.
2. How nodejs is a running environment on server side? what is VB?
3. What is the difference between running environment and framework?
4. What is the difference between nodejs & express.js
5. What is the difference between client-side(browser) & server-side(Nodejs). 

<!-- Question and aswer -->
# 1. What is nodejs.
Nodejs is neither a lungge not a framework.
Node/Node.js a runtime environment for excuting javascript code on the server side.

# 2. How nodejs is a running environment on server side? what is VB?
Browser executiing javascript on the client side,and similarly, Node.js executes javascript on the server side.
VB is a javascript engine for the javascript languge. 

# 3. What is the difference between running environment and framework?
Runtime environment:: primarily focuses on providing the necessary infrastructure for code execution , including service like memory management and input output operation.

Framework :: Primarily focus on simplifying the development process by offering a structured set of tools, libraries ,and best practies.

# 4. What is the difference between nodejs & express.js
Node.js is a runtime environment that allows the execution of JavaScript code server-side.

Express.js is a Framework build on the top Node.js.
      It is designed to simplify the process of building web application and APIS by providing a set of features like simple routing system,Middleware support etc.


# 5. What is the difference between client-side(browser) & server-side(Nodejs). 
|                                        |  client-side(browser)          |  server-side(Nodejs)                       |
|Environment location                    | Run on the users web browser.  | Run on the                                 |
|Primary Language                        | Html,css,javascript            | Javascript                                 |
|Document/Window/Navigator/Event Object  | Yes                            | No                                         |
|Request/Response/Server/Database Object | No                             | Yes                                        |
|Responsibilties                         | Handle ui display,intractions, | Handles business 
                                         |and client-side logic.          |logic ,data storage/access,authentication,
                                         |                                | authorization etc. 


<!-- 2. Main Features of Node.js -->
6. What are the 7 main features of node.js?
7. What is Single Threaded Programming?
8. What is Synchronouse Programming?
9. What is Multi Thread Programming?
10. What is asynchronous Programming?
11. What is difference bitween Synshronous and Asynchronouse Programming?
12. What are Events,Event Emitter,Event Queue,Event Loop & Event Driven?
13. What are the main features & advantages of node.js?
14. what are the disadvantage of node?

# 6. What are the 7 main features of node.js?
1. Single Threaded
2. Asynchrououse
3. Event-Driven
4. V8 javascript Engine
5. Cross-Plateform
6. NPM(Node Package Manager)
7. Real-Time Capabilities

# 7. What is Single Threaded Programming?
ye line by line code execute hoga aaur jab pura code execute hoga to wo usse bataiga ki code execute ho chuka hai .ye synchronous hi chalega.

# 8. What is Synchronouse Programming?
One task at a time.
In a synchronous program,each task is performed one after the other,and the program waits for each operation to compleate before moving on the next one.

Synchronouse programming focus on the order of execution in a sequential manner,while single-threaded programming focuses on the single threaded.


# 9. What is Multi Thread Programming?
dead lock problem
In a multi-threaded application ,multiple threads can execute multiple tasks simultaneously and parallelly.

As soon as one thread start a task,the next task in immediately initiated by creating a seconf=d thread.

# 10. What is asynchronous Programming?
non blocking

In node.js asynchronouse flow can be achieved by its single-thread,non-blocking, and event-driven architecture.

In Node.js ,if there are 4 task(task1,task2,task3,task4) to be compleated for an event.The below steps will be executed.
1. First,  Thread T1 will be created.
2. Thread T1 initiates,Task1 ,but it won't wait for task1 to compleate.Instead T1 proceeds to iniate Task2,Then Task3 and Task4(This asynchronouse execution allow T1 to efficiently handle multiple task concurrently).
3. Whenever Task1 compleated an event is emmited.
4. Thread T1,being event-driven,prompty responds to this events,interrupting its current task and delivering the result of task1.


# 11. What is difference bitween Synshronous and Asynchronouse Programming?
1. Synshronous :: In synchronous programming ,task are executed one after another in a sequential manner.
Asynchronouse  :: In sysnchronouse programminf=g ,task can start,run and complate in parallel.
2. Synshronous :: Each task must be compleated before the program move on to the next task.
Asynchronouse  :: Task can be executed independently of each other.
3. Synshronous :: Each of code is blocked until a task is finished.
Asynchronouse  :: Asynchronouse operations are typically non-blocking.
4. Synshronous ::Synchronouse operation can lead to blocking and unresponsiveness.
Asynchronouse  :: It enables better concurrancy and responsiveness.

# 12. What are Events,Event Emitter,Event Queue,Event Loop & Event Driven?
Events       ::Single that something has happened in a program.

Event Emitter::Create or emit events.

Event Queue  :: Event emitted queued(stored) in event queue.

Event Handler(Event Lister):: Function that responds to specific events.

Event Loop :: The event loop pick up event from the event queue and executes them in the order the were added.

Event Driven Architecture :: It means operations in Node are drive or based by events.

# 13. What are the main features & advantages of node.js?
1. Single Threaded =>
2. Asynchrououse =>Enable handling multiple concurrency request & non blocking execution of thread.
3. Event-Driven => Efficient handling events.Great for real time application like chat applications,gaming application(using web sockets) where bidirection communication is required.
4. V8 javascript Engine => Build on the v8 js engine from google chrome,Node,js executes code fast.
5. Cross-Plateform => support deployment on various operating systems,enhanceing flexibility.
6. NPM(Node Package Manager) =>
7. Real-Time Capabilities =>
8. Javascript => Coding in js language there for no need to learn a new language
9.            =>Suitable for building scalable application that can handle increase loads.

# 14. what are the disadvantage of node?

When to use Node.js =>
1. Ideal for real time application like chat applications ,online gaming and collaborative tools due to its event-drive architecture.
2. Excellenent for building lightweight and scalable restful APIS that handle a large number of concurrent connections.
3. Well-suitable for building miroservices-based architectures,enabling modular and scable system.

When not to use Node.js(Disadvantage)=>
Cpu-Intensive Task :: Avoid application that involve heavy cpu processing(image/video processing,Data Encryption/Descryption) as node.js may not provide optimal performance in such scenarios because it single threaded and for heavy computation multi-threaded is better.

<!-- 3.Project Setup & Modules -->
15. How to setup node.js project?
16. What is NPM.what is the role of node_module folder?
17. what is the role of package.json file in node.js?
18. what are modules in node?whats is the difference between a function & module?
19. How many way are there to Export a module?
20. what will happen if you don't export in module?
21. How to import single and multiple function from module?
22. what is module wrapper function?
23. what are the types of module in nodejs?

# 15. How to setup node.js project?
7 step for setting up node.js in vs code.
1. Download node.js installer & install it.
2. Download vscode editor install & install it.
3. create a new folder anywhere.
4. open vscode and open project folder from it.
5. open terminal in vscode and run this command 
npm init -y
6. create new js file(app.js) within your project folder(proectname).
7. run a project
npm install
node app.js

# 16. What is NPM.what is the role of node_module folder?
NPM(Node Package Manager) is used to manage the dependencies for your Node project.

node_modules folder cantains all the dependencies of the node project.

# 17. what is the role of package.json file in node.js?
The package.json the file cantains project metadata(information about the project).
For example,project name,version,description,author,licence,script etc.

# 18. what are modules in node?whats is the difference between a function & module?
A module contains a specific function that can be easily reused within a node.js application.

Ideally in node,js,a javascript file can be treated as a module
<!-- myModule.js -->
function sayHello(name){
    console.log("Hello," + name);
}

module.exports = sayHello;

1. A module is a broader concept that  encapsulates functionality,while a function is a specific set of instructions within that module.
2. Module can cantain multiple function and variable.

# 19. How many way are there to Export a module?

# First method ::
module exports object can be used to expor the module.
<!-- Exporting a function -->
function sayHello(name){
    console.log("Hello," + name);
}

<!-- Exporting a variable -->
const pi = 3.14159;

<!-- Export a object -->
const myObject = {
    key:"value",
};

<!-- Using module.export -->
module.exports.sayHello = sayHello;
module.exports.pi = pi;
module.export.myObject = myObject;
 
# Second Method ::
export object can be also used directly to export the module.
<!-- Exporting a function -->
exports.sayHello(name){
    console.log("Hello," + name);
}

<!-- Exporting a variable -->
exports.pi = 3.14159;

<!-- Export a object -->
exports.myObject = {
    key:"value",
};


# 20. what will happen if you don't export in module?
If you don't export the module,then the module function will not be available outside the module.

# 21. How to import single and multiple function from module?
In node.js, you can import a module using the require function.

<!-- Exporting a function  module2.js-->
function sayHello(name){
    console.log("Hello," + name);
}

<!-- To export single function -->
module.exports.sayHello = sayHello;

<!-- To export multiple function -->

<!-- module.exports.sayHello = sayHello;
module.exports.pi = pi;
module.export.myObject = myObject; -->

<!-- app.js -->
<!-- Import the module2 module -->
const module2 = require('./module2');

<!-- To import single function -->
module2('Happy')
<!-- output :: Hello ,Happy -->

<!-- To import multiple function -->
module2.sayHello('Happy';)

22. what is module wrapper function?
In Node.js, each module is wrapped in a function called the "module wrapper function " before it is executed.

23. what are the types of module in nodejs?
3 type of module
1. Build-in Module(core module) :: These are already present models in node.js Which provide essential functionalities.for example fs(file system),http(https server),path(path manipulation) and Util(utilities)
2. Local Modules : This user-defined-modules(js files) created by developer for specific functionalities.
3. Third-Party Modules : These are external package or libraries created by the community and provid additional functionality for node project. you install third party modules using the npm install command.
 for example, npm install lodash

<!-- 4.Top built in modules -->
 24. What are the top 5 built in model command used in node projects?
 25. Explain the role of fs module? Name some function of it?
 26. Explain the role of path module? Name some function of it?
 27. Explain the role of OS module? Name some function of it?
 28. Explain the role of Events module? How to handle events in node?
 29. What are Event Arguments?
 30. Whats is the difference between a function and an event?
 31. Whats is the role of http module in node?
 32. Whats is the role createServer() method of http module

# 24. What are the top 5 built in model command used in node projects?
1. fs
2. path
3. os
4. events
5. http

# 25. Explain the role of fs module? Name some function of it?
fs(file system) module in node provides a set of method for interacting with the file system.

# 7 main function of fs module ::
1. fs.readFile() ::Read the content of the file specified.
2. fs.writeFile() :: Write data to the specified file,creating the file if it does not exits.
3. fs.appendFile() :: Append data to a file.if the file does not exist,it is created.
4. fs.unlink() :: Deletes the specified file.
5. fs.readdir():: Read the contents of a directory.
6. fs.mkdir():: Create a new directory.
7.fs,rmdir():: Remove the specified directory.
<!-- fs-example.js -->
const fs = require("fs")
<!-- Reading the contents of a file asynchronously -->
fs.readFile("fs.txt","utf8",(err,data)=>{
    if(err){
        return;
    }
    console.log("File content :", data)
});

<!-- write to a file asynchronously -->
const contentToWrite = "Some content";
fs.writeFile("fs.txt",contentToWrite,"utf8",(err)=>{
    if(err){
        return;
    }
    console.log("Write operation compleate")
});



 26. Explain the role of path module? Name some function of it?

 # 27. Explain the role of OS module? Name some function of it?
 The os module in Node.js provides a set of methods for interacting with the operating system.

 Operating system can be used by developers for building cross-plateform application or performing system-related tasks in Node.js applications.

 const os = require("os");
 <!-- 1. Fet Plateform information -->
 console.log(os.type());
 <!-- output: 'Window_NT' or Linux -->

 <!-- 2. Get current user information -->
 console.log(os.userInfo());
 <!-- output::{uid:-1,gid:-1,username:'dev'} -->

 <!-- 3.Get Memory Information in bytes -->
 console.log(os.totalmem());
 console.log(os.freemem());

 # 28. Explain the role of Events module? How to handle events in node?

 1. Event Module is used to handle events.
2. Event

 # 29. What are Event Arguments?
Event arguments refer to the additional information or data that can be passed along with an emitted event.

const EventEmitter = require("events");

<!-- Create an instance of EventEmitter class -->
const myEmitter = new EventEmitter();

<!-- Register an event listner for the 'eventName' event-->
myEmitter.on("eventName",(arg1,arg2)=>{
    console.log("Event occurred with arguments:",arg1,arg2)
});

<!-- Email the 'event' with argument -->
myEmitter.emit("eventName","Arg 1"," Arg 2")

<!-- output::Event occurred with arguments:Arg1 Arg2  -->

 # 30. Whats is the difference between a function and an event?
A function is a reusable piece of code that performs a specific task task when invoked or called.

Events represent actions that can be observed and responded to.Events will call functions internallty.

 # 31. Whats is the role of http module in node?
The http module can create an http server that listner to server port and gives a response back to client.

 # 32. Whats is the role createServer() method of http module
 The createServer() method of the http module in Node.js is used to create an Http server.

 <!-- createServer.js -->
 <!-- 1.Import the 'http' moduleto create an http server -->
 const  http =require('http');

 <!-- 2.Create an HTTP server using the 'createServer' method -->
 const server = http.createServer((req,res)=>{
    <!-- Handle incoming Http request here -->
    res.send("Hello ,World")
 });

 <!-- 3. Set port on which server  will listen incoming request -->
 const port = 3000;

 <!-- 4.Start the server and listen on the specific port -->
 server.listen(port,()=>{
    console.log(`Server listing on port ${PORT}`);
 })

 <!-- 5.Run command in terminal : node createServer.js -->